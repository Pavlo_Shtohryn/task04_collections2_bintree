package com.shtohryn;


import java.util.ArrayList;
import java.util.Scanner;

/**
 * Main class with:
 * 1. adding an element;
 * 2. deleting an element;
 * 3. execution of the request;
 * 4. print the collection.
 */
public class Main {

    private static Scanner in;
    private static BinaryTree three = new BinaryTree();

    /**
     * main function with bar menu
     * @param args
     */
    public static void main(String[] args) {
        in = new Scanner(System.in);
        Abiturient a1 = new Abiturient("name-one", "male", "address", "fac", 1, 5, 5, 5);
        three.add(a1);
        three.add(new Abiturient("name-two", "male", "address", "fac", 3, 5, 5, 5));

        three.add(new Abiturient("name-three", "male", "address", "fac", 4, 5, 5, 5));
        three.add(new Abiturient("name-four", "male", "address", "fac", 6, 5, 5, 5));
        three.add(new Abiturient("name-five", "male", "address", "fac", 1, 5, 5, 5));
        three.add(new Abiturient("name-six", "male", "address", "fac", 2, 5, 5, 5));
        three.add(new Abiturient("name-seven", "male", "address", "fac", 3, 5, 5, 5));
        three.add(new Abiturient("name-eight", "male", "address", "fac", 4, 5, 5, 5));
        three.add(new Abiturient("name-nine", "male", "address", "fac", 1, 5, 5, 5));
        three.add(new Abiturient("name-ten", "male", "address", "fac", 3, 5, 5, 5));


        while (true) {
            System.out.println("<--------------------------------------------------------->");
            System.out.println("Enter the comand");
            System.out.println("add -for adding item");
            System.out.println("delete to  delete item by key");
            System.out.println("'find'- to request");
            System.out.println("'output' - to print list ");
            System.out.println("<--------------------------------------------------------->");
            String command = "";
            command = in.nextLine();
            switch (command) {
                case "add":
                    add();
                    break;
                case "delete":
                    if (!three.isEmpty()) delete();
                    else System.out.println("Binary tree consists items");
                    break;
                case "find":
                    Object[] temp = three.toArray();
                    if (temp.length == 0) {
                        System.out.println("Binary tree doesn't consists item");
                        break;
                    }
                    int numSch;
                    while (true) {
                        System.out.println("Enter the school's number");
                        try {
                            numSch = Integer.parseInt(in.nextLine());
                            if (numSch > 0) break;
                        } catch (NumberFormatException e) {
                            System.out.println("Not correct  number ");
                        }
                    }
                    final int num = numSch;
                    ArrayList<Abiturient> abiturients = new ArrayList<Abiturient>();

                    for (int i = 0; i < temp.length; i++) abiturients.add((Abiturient) temp[i]);
                    abiturients.forEach(System.out::println);
                    break;
                case "output":
                    Object[] string = three.output();
                    for (Object s : string) {
                        System.out.println(s);
                    }
                    break;
                default:
                    System.out.println("ERROR unknown command. Try again");
                    break;
            }
        }
    }

    private static void add() {
        String name, gender, address, facultet, temp;
        int numSch;
        int[] bal = new int[3];
        do {
            System.out.println("Enter the abiturient's name ");
            name = in.nextLine();
        } while (!correctString(name) || containsNum(name));

        do {
            System.out.println("Enter the abiturient's sex");
            gender = in.nextLine();
        } while (!correctGender(gender) || containsNum(gender));

        do {
            System.out.println("Enter th abiturient's address");
            address = in.nextLine();
        } while (!correctString(address));

        do {
            System.out.println("Enter  abiturient's faculty");
            facultet = in.nextLine();
        } while (!correctString(facultet));

        while (true) {
            System.out.println("Enter abiturient's school's number");
            temp = in.nextLine();
            try {
                numSch = Integer.parseInt(temp);
                break;
            } catch (NumberFormatException ex) {
                System.out.println("Not correct  number ");
            }
        }

        while (true) {
            System.out.println("Enter the grades for the three exams through the space");
            try {
                bal[0] = in.nextInt();
                bal[1] = in.nextInt();
                bal[2] = in.nextInt();
                in.nextLine();
                break;
            } catch (Exception e) {
                System.out.println("Not correct  number ");
                in.nextLine();
            }
        }
        while (true) {
            Abiturient a = null;
            System.out.println("Applicant studying on a commercial form? y/n");
            temp = in.nextLine();
            if (equals(temp, "true", "y")) {
                int cost_learn;
                while (true) {
                    System.out.println("Enter the cost of the stading");
                    temp = in.nextLine();
                    try {
                        cost_learn = Integer.parseInt(temp);
                        break;
                    } catch (NumberFormatException ex) {
                        System.out.println("Not correct  number ");
                    }
                }
                a = new Abiturient(name, gender, address, facultet, numSch, bal[0], bal[1], bal[2], true, cost_learn);
            } else a = new Abiturient(name, gender, address, facultet, numSch, bal[0], bal[1], bal[2]);
            three.add(a);
            break;
        }
    }

    private static void delete() {
        int n = 0;
        while (true) {
            System.out.println("Enter the index of the item in the collection to delete it ");
            try {
                n = Integer.parseInt(in.nextLine());
                System.out.println("element deleted " + three.delete(n));
                break;
            } catch (NumberFormatException e) {
                System.out.println("Not correct number");
            } catch (NullPointerException e) {
                System.out.println("An element in such an index does not exist in the collection");
            }
        }
    }

    private static ArrayList<Abiturient> find() {
        ArrayList<Abiturient> result = new ArrayList<>();
        return result;
    }

    public static boolean correctString(String s) {
        return (!s.equals("") && !s.equals(" "));
    }

    public static boolean containsNum(String s) {
        for (int i = 0; i < s.length(); i++) {
            try {
                Integer.parseInt(String.valueOf(s.charAt(i)));
                return true;
            } catch (NumberFormatException ex) {
                continue;
            }
        }
        return false;
    }

    public static boolean correctGender(String s) {
        return (s.equals("male") || s.equals("female"));
    }

    public static boolean equals(String string, String... answers) {
        for (int i = 0; i < answers.length; i++) {
            if (string.equals(answers[i])) return true;
        }
        return false;
    }
}